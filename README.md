Ablauf:
======

1. Person füllt Formular (e-mail) aus
2. Info mit Kontakt wird an Kampagnenverantwortliche geschickt
3. Bestätigung der Registration
4. Eintrag in Newsletter => Automatisiertes Wilkommensmail mit Anleitungen
5. Regelmässige Kampagnenmails durch Kampagnenteam

Funktionen:
- Keycloak
- Mailchimp/Mailtrain
- Kontaktdatenbank mit Synchronisation zu Mailchimp/Keycloak
- Formulare lassen sich einbinden
- Counter für Einbindung
- Custom webhooks auf Registration

Skript für Einbindung als Formular, CSS, Mit Codes um Herkunft zu bestimmen.

Landing-Pages? => Seperates Tool und Einbettung.
