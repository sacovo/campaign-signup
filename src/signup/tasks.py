from datetime import timedelta
import traceback
from celery import shared_task
from django.utils import timezone

from mailchimp3.mailchimpclient import MailChimpError
from signup.models import Activist, KeycloakConnection, never


def register_mailchimp(activist: Activist):
    if activist.source.mailchimp and not activist.source.disable_mailchimp:
        try:
            activist.source.mailchimp.register_user(activist)
        except Exception as e:
            return str(e)


def process_webhooks(activist):

    for webhook in activist.source.webhooks.all():
        try:
            webhook.send(activist)
        except Exception as e:
            print(e)


def create_keycloak_user(activist):
    if activist.source.keycloak:
        activist.source.keycloak.register_user(activist)


@shared_task
def update_password_times(keycloak_id):
    KeycloakConnection.objects.get(pk=keycloak_id).update_password_time()


@shared_task
def process_new_user(activist_pk):
    activist = Activist.objects.get(pk=activist_pk)

    create_keycloak_user(activist)
    process_webhooks(activist)
    try:
        register_mailchimp(activist)
    except MailChimpError as e:
        return str(e)


@shared_task
def update_and_tag_users(language_code):
    for keycloak in KeycloakConnection.objects.all():
        keycloak.update_user_ids()
        keycloak.update_password_time()

    for activist in Activist.objects.filter(
            source__mailchimp__isnull=False,
            password_created=never(),
            created_at__lt=timezone.now() - timedelta(days=2),
            reminders_sent=0,
            source__locale=language_code,
    ):
        try:
            activist.source.mailchimp.tag_user(activist, 'inactive')
            activist.reminders_sent += 1
            activist.save()
        except:
            traceback.print_exc()


@shared_task
def check_user_activation():
    """Checks all users that registered 2 days ago."""
    pass
