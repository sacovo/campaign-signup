from django.contrib import admin
import keycloak
from signup import tasks

from signup.models import (Activist, BlockedContact, KeycloakConnection,
                           MailchimpConnection, Source, Webhook,
                           WelcomeMessage, inactive_activists_longer_than,
                           never)

# Register your models here.


@admin.register(KeycloakConnection)
class KeycloakAdmin(admin.ModelAdmin):
    list_display = ['server_url', 'realm', 'client_id', 'user_realm_name']
    search_fields = ['server_url', 'realm']

    actions = ['update_users', 'update_password_times']

    def update_users(self, request, queryset):
        for keycloak_connection in queryset:
            keycloak_connection.update_user_ids()

    def update_password_times(self, request, queryset):
        for keycloak_connection in queryset:
            tasks.update_password_times.delay(keycloak_connection.pk)


@admin.register(MailchimpConnection)
class MailchimpAdmin(admin.ModelAdmin):
    list_display = ['list_name', 'server_prefix', 'list_id']
    search_fields = ['list_name']


@admin.register(WelcomeMessage)
class WelcomeMessageAdmin(admin.ModelAdmin):
    search_fields = ['welcome_message']


@admin.register(Webhook)
class WebhookAdmin(admin.ModelAdmin):
    list_display = ['url', 'basic_auth_user']

    search_fields = ['url']


@admin.register(Source)
class SourceAdmin(admin.ModelAdmin):
    fields = [
        'tag', 'keycloak', 'mailchimp', 'disable_mailchimp', 'webhooks',
        'welcome_message', 'locale', 'id', 'embed_code'
    ]
    list_display = ['tag', 'id']
    readonly_fields = ['id', 'embed_code']
    search_fields = ['tag']

    autocomplete_fields = [
        'keycloak', 'mailchimp', 'webhooks', 'welcome_message'
    ]


class ActiveFilter(admin.SimpleListFilter):
    title = "registration"
    parameter_name = "register"

    def lookups(self, request, model_admin):
        return (
            ("active", "active"),
            ("inactive", "inactive"),
        )

    def queryset(self, request, queryset):
        if self.value() == 'active':
            return queryset.filter(password_created__gt=never())
        if self.value() == 'inactive':
            return queryset.filter(password_created=never())


@admin.register(Activist)
class ActivistAdmin(admin.ModelAdmin):
    list_display = [
        'username', 'first_name', 'last_name', 'zip_code', 'phone',
        'created_at'
    ]
    fields = [
        'username', 'email', 'first_name', 'last_name', 'zip_code', 'phone',
        'keycloak_id', 'created_at', 'password_created', 'source',
        'reminders_sent'
    ]
    readonly_fields = ['created_at']
    search_fields = ['username', 'first_name', 'last_name']
    list_filter = ['password_created', 'source', 'created_at', ActiveFilter]
    date_hierarchy = 'created_at'
    actions = ['send_password_update', 'mark_inactive', 'unmark_selected']

    def send_password_update(self, request, queryset):
        if queryset.exists():
            queryset.first().source.keycloak.update_password(queryset, request)

    def mark_inactive(self, request, queryset):
        queryset = inactive_activists_longer_than(queryset).exclude(
            source__mailchimp__isnull=True, )

        for inactive in queryset:
            print(inactive)
            inactive.source.mailchimp.tag_user(inactive, 'inactive')

    def unmark_selected(self, request, queryset):
        for active in queryset.exclude(source__mailchimp__isnull=True):
            active.source.mailchimp.untag_user(active, 'inactive')


@admin.register(BlockedContact)
class BlockedContactAdmin(admin.ModelAdmin):
    list_display = ['name', 'email_regex', 'name_regex', 'is_active']
