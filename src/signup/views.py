import json
from django.core.exceptions import PermissionDenied
from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from signup.forms import SignupForm
from signup.models import Activist, Source
from signup.tasks import process_new_user

# Create your views here.


@csrf_exempt
def signup(request, pk):
    if request.method != "POST":
        return render(request, "signup/signup_form.html", {
            'form': SignupForm(),
            'pk': pk
        })

    source = Source.objects.get(pk=pk)

    if request.POST:
        form = SignupForm(request.POST)
    else:
        form = SignupForm(json.loads(request.body))

    if not form.is_valid():
        return render(request, "signup/signup_form.html", {
            'form': form,
            'pk': pk
        })

    activist = Activist.objects.create(
        username=form.cleaned_data['email'],
        email=form.cleaned_data['email'],
        first_name=form.cleaned_data['first_name'],
        last_name=form.cleaned_data['last_name'],
        phone=form.cleaned_data['phone'],
        zip_code=form.cleaned_data['zip_code'],
        source=source,
    )

    process_new_user.delay(activist.pk)

    return HttpResponse(
        source.welcome_message.display.format(activist=activist))


def signup_test(request, pk):
    return render(request, "signup/signup_test.html", {
        'pk': pk,
    })


def count(request):
    return HttpResponse(str(Activist.objects.all().count()))
