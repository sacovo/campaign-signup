from datetime import datetime, timedelta
import traceback
import uuid
from django.contrib import messages
from mailchimp3.helpers import get_subscriber_hash

import requests
from django.contrib.sites.models import Site
from django.db import models
from django.template.loader import render_to_string
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

import keycloak
from mailchimp3 import MailChimp

# Create your models here.


class KeycloakConnection(models.Model):
    """Connection to keycloak."""

    server_url = models.CharField(max_length=255, verbose_name=_("server url"))

    realm = models.SlugField(verbose_name=_("realm"))
    user_realm_name = models.CharField(max_length=255,
                                       verbose_name=_("user realm name"))
    client_id = models.CharField(max_length=255, verbose_name=_("client id"))
    client_secret_key = models.CharField(max_length=255,
                                         verbose_name=_("client secret"))

    def register_user(self, activist):
        keycloak_admin = self.get_admin()

        activist.keycloak_id = keycloak_admin.create_user({
            'username':
            activist.username,
            'email':
            activist.email,
            'firstName':
            activist.first_name,
            'lastName':
            activist.last_name,
            'attributes': {
                'locale': [activist.source.locale],
            },
            'enabled':
            True,
        })

        keycloak_admin.send_update_account(user_id=activist.keycloak_id,
                                           payload='["UPDATE_PASSWORD"]')

        activist.save()

    def update_password(self, activists, request):
        keycloak_admin = self.get_admin()
        for activist in activists:
            try:
                keycloak_admin.send_update_account(
                    activist.keycloak_id, payload='["UPDATE_PASSWORD"]')
            except:
                messages.add_message(request, messages.ERROR,
                                     "Failed to update: " + str(activist))
                traceback.print_exc()

    def update_user_ids(self):
        keycloak_admin = self.get_admin()
        users = keycloak_admin.get_users({})
        for user in users:
            activist = Activist.objects.filter(
                email=user['email'],
                keycloak_id="",
            ).first()

            if activist:
                activist.keycloak_id = user['id']
                activist.save()

    def update_password_time(self):
        keycloak_admin = self.get_admin()
        for activist in Activist.objects.filter(source__keycloak=self):
            credentials = keycloak_admin.raw_get(
                "admin/realms/" + self.realm + "/users/" +
                activist.keycloak_id + "/credentials", ).json()

            for credential in credentials:
                if type(credential) == dict and credential.get(
                        'type', '') == 'password':
                    activist.password_created = timezone.make_aware(
                        datetime.fromtimestamp(credential['createdDate'] /
                                               1000))
                    activist.save()

    def get_admin(self):
        return keycloak.KeycloakAdmin(
            server_url=self.server_url,
            client_id=self.client_id,
            client_secret_key=self.client_secret_key,
            realm_name=self.realm,
            user_realm_name=self.user_realm_name,
        )

    def __str__(self):
        return self.server_url

    class Meta:
        verbose_name = _("keycloak connection")
        verbose_name_plural = _("keycloak connections")


class MailchimpConnection(models.Model):
    """Connection to mailchimp"""
    list_name = models.CharField(max_length=255, verbose_name=_("list name"))
    server_prefix = models.CharField(max_length=10,
                                     verbose_name=_("server prefix"))
    api_key = models.CharField(max_length=255, verbose_name=_("api key"))
    list_id = models.CharField(max_length=50, verbose_name=_("list id"))
    plz_merge = models.CharField(max_length=50, verbose_name=_("plz merge"))

    def register_user(self, activist):
        client = MailChimp(mc_api=self.api_key)
        activist.mailchimp_id = client.lists.members.create(
            self.list_id, {
                'email_address': activist.email,
                'status': 'subscribed',
                'merge_fields': {
                    'FNAME': activist.first_name,
                    'LNAME': activist.last_name,
                    'PHONE': activist.phone,
                    self.plz_merge: activist.zip_code
                }
            })['id']
        activist.save()

    def tag_user(self, activist, tag):
        client = MailChimp(mc_api=self.api_key)
        client.lists.members.tags.update(
            list_id=self.list_id,
            subscriber_hash=get_subscriber_hash(activist.email),
            data={'tags': [{
                'name': tag,
                'status': "active"
            }]},
        )

    def untag_user(self, activist, tag):
        client = MailChimp(mc_api=self.api_key)
        client.lists.members.tags.update(
            list_id=self.list_id,
            subscriber_hash=get_subscriber_hash(activist.email),
            data={'tags': [{
                'name': tag,
                'status': "inactive"
            }]},
        )

    def __str__(self):
        return self.list_name

    class Meta:
        verbose_name = _("mailchimp connection")
        verbose_name = _("mailchimp connections")


class WelcomeMessage(models.Model):
    """Message that is sent to new signups."""
    display = models.TextField(
        verbose_name=_("message"),
        help_text=_("use {activist.first_name}, ... as placeholders."))

    def __str__(self):
        return self.display[:50]

    class Meta:
        verbose_name = _("welcome message")
        verbose_name = _("welcome messages")


class Webhook(models.Model):
    """Called with information about the new users."""
    url = models.URLField(verbose_name=_("url"))

    basic_auth_user = models.CharField(max_length=255,
                                       verbose_name=_("basic auth user"))
    basic_auth_password = models.CharField(
        max_length=255, verbose_name=_("basic auth password"))

    def send(self, activist: "Activist"):
        data = {
            'first_name': activist.first_name,
            'last_name': activist.last_name,
            'email': activist.email,
            'plz': activist.zip_code,
            'phone': activist.phone,
            'keycloak_id': activist.keycloak_id,
            'pk': activist.pk,
        }

        if self.basic_auth_password:
            requests.post(self.url,
                          auth=(self.basic_auth_user,
                                self.basic_auth_password),
                          data=data)
        else:
            requests.post(self.url, data=data)

    def __str__(self):
        return self.url

    class Meta:
        verbose_name = _("webhook")
        verbose_name_plural = _("webhooks")


class Source(models.Model):
    """A form that generates new entries."""
    tag = models.SlugField(unique=True, verbose_name=_("tag"))
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)

    keycloak = models.ForeignKey(KeycloakConnection,
                                 models.CASCADE,
                                 null=True,
                                 blank=True)
    mailchimp = models.ForeignKey(MailchimpConnection,
                                  models.CASCADE,
                                  null=True,
                                  blank=True)
    disable_mailchimp = models.BooleanField(default=False)
    welcome_message = models.ForeignKey(WelcomeMessage,
                                        models.CASCADE,
                                        null=True,
                                        blank=True)
    webhooks = models.ManyToManyField(Webhook, blank=True)
    locale = models.CharField(max_length=12)

    def __str__(self):
        return self.tag

    @property
    def embed_code(self):
        return mark_safe("<pre>" + escape(
            render_to_string("signup/embed_code.html", {
                'pk': self.pk,
                'site': Site.objects.get_current()
            })) + "</pre>")

    class Meta:
        verbose_name = _("source")
        verbose_name_plural = _("sources")


def never():
    return timezone.make_aware(datetime.fromtimestamp(0))


def inactive_activists_longer_than(queryset):
    return queryset.filter(password_created=never(), )


class Activist(models.Model):
    """Result of the entires."""
    username = models.CharField(max_length=255, unique=True)
    email = models.EmailField()

    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)

    zip_code = models.CharField(max_length=20, blank=True)
    phone = models.CharField(max_length=30, blank=True)

    keycloak_id = models.CharField(max_length=255, blank=True)
    mailchimp_id = models.CharField(max_length=255, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    password_created = models.DateTimeField(default=never)

    source = models.ForeignKey(
        Source,
        models.CASCADE,
    )

    reminders_sent = models.IntegerField(default=0)
    activated_keycloak = models.BooleanField(default=False)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = _("activist")
        verbose_name_plural = _("activists")


class BlockedContact(models.Model):
    name = models.CharField(max_length=255)
    email_regex = models.CharField(max_length=255)
    name_regex = models.CharField(max_length=255)

    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("blocked contact")
        verbose_name_plural = _("blocked contacts")
