const SCRIPT_ORIGIN = new URL(document.currentScript.src).origin;

const BASE_URL = SCRIPT_ORIGIN;

function initElement(element) {
  const url = BASE_URL + "/" + element.dataset.id + "/";

  fetch(url)
    .then((response) => response.text())
    .then((text) => {
      element.innerHTML = text;
      const submitHandler = (event) => {
        event.preventDefault();
        fetch(event.target.action, {
          method: "POST",
          body: new FormData(event.target),
        })
          .then((response) => response.text())
          .then((text) => (element.innerHTML = text))
          .then(() => {
            let form = document.getElementById(`form-${element.dataset.id}`);
            if (form) {
              form.addEventListener("submit", submitHandler);
            }
          });
        return false;
      };

      document
        .getElementById(`form-${element.dataset.id}`)
        .addEventListener("submit", submitHandler);
    });
}

if (document.readyState === "loading") {
  document.addEventListener("DOMContentLoaded", () => {
    const elements = document.getElementsByClassName("SignupForm");
    Array.prototype.forEach.call(elements, (element) => {
      initElement(element);
    });
  });
} else {
  const elements = document.getElementsByClassName("SignupForm");

  Array.prototype.forEach.call(elements, (element) => {
    initElement(element);
  });
}
