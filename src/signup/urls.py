from django.urls import path

from signup import views

app_name = "signup"

urlpatterns = [
    path("<uuid:pk>/", views.signup, name="signup"),
    path("count/", views.count, name="count"),
    path("test/<uuid:pk>/", views.signup_test, name="signup_test")
]
