import phonenumbers
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from phonenumbers.phonenumberutil import NumberParseException

from signup.models import Activist


def unique_email(value):
    if Activist.objects.filter(email__iexact=value).exists():
        raise ValidationError(_("%(value)s is already registered"),
                              params={'value': value})


class SignupForm(forms.Form):
    """Form that is displayed on landing pages."""

    first_name = forms.CharField(label=_("first name"))
    last_name = forms.CharField(label=_("last name"), required=False)
    email = forms.EmailField(label=_("email"), validators=[unique_email])
    zip_code = forms.CharField(label=_("zip"), required=False)

    phone = forms.CharField(label=_("phone"), required=False)

    def clean_phone(self):
        if self.cleaned_data['phone'] == '':
            return ''

        try:
            parsed = phonenumbers.parse(self.cleaned_data['phone'], "CH")
        except NumberParseException:
            raise ValidationError(_("%(value)s is not a valid phone number."),
                                  params={'value': self.cleaned_data['phone']})

        if not phonenumbers.is_valid_number(parsed):
            raise ValidationError(_("%(value)s is not a valid phone number"),
                                  params={'value': self.cleaned_data['phone']})

        return phonenumbers.format_number(parsed,
                                          phonenumbers.PhoneNumberFormat.E164)
